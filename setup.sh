D="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# zsh
ln -sf $D/zshrc ~/.zshrc

# vim
mkdir -p ~/.vim
echo 'source ~/.vim/rc.vim' > ~/.vimrc
ln -sf $D/vim/rc.vim ~/.vim/rc.vim
ln -sf $D/vim/vim-plug.vim ~/.vim/vim-plug.vim
