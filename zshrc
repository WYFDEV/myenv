# MyHome
export H=/home/wyf
export W=/work



#######################################################
# oh-my-zsh setting
#######################################################

# Path to your oh-my-zsh installation.
export ZSH=$H/.oh-my-zsh

# Set name of the theme to load.
ZSH_THEME="robbyrussell"

# CASE_SENSITIVE="true"
# HYPHEN_INSENSITIVE="true"
# DISABLE_AUTO_UPDATE="true"
# export UPDATE_ZSH_DAYS=13
# DISABLE_LS_COLORS="true"
# DISABLE_AUTO_TITLE="true"
# ENABLE_CORRECTION="true"
# COMPLETION_WAITING_DOTS="true"
# DISABLE_UNTRACKED_FILES_DIRTY="true"
# HIST_STAMPS="mm/dd/yyyy"

# (plugins can be found in ~/.oh-my-zsh/plugins/*)
plugins=(
            sudo, git, systemd, httpie, docker, adb,
            golang,
            gcloud,
            ruby, rbenv,
            python, pip, pyenv, virtualenv, virtualenvwrapper, djange,
            node, npm,
            laravel5, composer
        )

# init oh-my-zsh
source $ZSH/oh-my-zsh.sh

# export LANG=en_US.UTF-8

# export ARCHFLAGS="-arch x86_64"
# export SSH_KEY_PATH="~/.ssh/dsa_id"



#######################################################
# envriment
#######################################################

# pip zsh completion
function _pip_completion {
  local words cword
  read -Ac words
  read -cn cword
  reply=( $( COMP_WORDS="$words[*]" \
             COMP_CWORD=$(( cword-1 )) \
             PIP_AUTO_COMPLETE=1 $words[1] ) )
}
compctl -K _pip_completion pip


# append to path
function path_append {
    export PATH="$PATH:$1"
}

# set proxy
function proxy_switch {
    if [ -z "$http_proxy" ]; then
        export http_proxy='http://127.0.0.1:9001'
        export https_proxy='http://127.0.0.1:9001'
    else
        unset http_proxy
        unset https_proxy
    fi
    echo 'http[s]_proxy: '     $http_proxy
}


export EDITOR='gvim'


# keep table path
. /etc/profile.d/vte.sh


# PATH CONFIG
export PATH=$PATH
path_append ~/.bin


# Golang setting
export GOPATH=~/go
export GOARCH=amd64
export GOOG=linux
export GOBIN=$PATH/bin
path_append $GOBIN
[ -f "$H/.gvm/scripts/gvm" ] && source "$H/.gvm/scripts/gvm"


alias vi="gvim 2>/dev/null"
alias g=git
alias d=docker
alias hk=heroku


[ -f "$H/.zone/rc.sh" ] && source "$H/.zone/rc.sh"


# pyenv setting
# export PYENV_ROOT="$H/.pyenv"
# export PATH="$PYENV_ROOT/bin:$PATH"
# eval "$(pyenv init -)"
# eval "$(pyenv virtualenv-init -)"   # 这句可以不加


# virtualenvwrapper setting
if [ -f  /usr/bin/virtualenvwrapper.sh ]; then
    export WORKON_HOME=~/.venvs
    source /usr/bin/virtualenvwrapper.sh
fi


# rbenv setting
if [ -f $H/.rbenv ]; then
    export RBENV_ROOT="$H/.rbenv"
    path_append $RBENV_ROOT/bin:$PATH
    eval "$(rbenv init -)"
    # exec rbenv
    # exec "$(basename "$0")" "$@"
fi


# node & npm
# export NODE_PATH="/usr/lib/node_modules"
alias npm="npm --registry=https://registry.npm.taobao.org \
        --cache=$H/.npm/.cache/cnpm \
        --disturl=https://npm.taobao.org/dist \
        --userconfig=$H/.npmrc"
export NODE_PATH=$(npm root --quiet -g)
# nvm <https://github.com/creationix/nvm#installation>
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm


# Google cloud about
# The next line updates PATH for the Google Cloud SDK.
if [ -f "$H/.local/google-cloud-sdk/path.zsh.inc" ]; then
    source "$H/.local/google-cloud-sdk/path.zsh.inc";
fi

# The next line enables shell command completion for gcloud.
if [ -f "$H/.local/google-cloud-sdk/completion.zsh.inc" ]; then
    source "$H/.local/google-cloud-sdk/completion.zsh.inc";
fi

# go appengine
if [ -d "$H/.softs/go_appengine" ]; then
    path_append "$H/.softs/go_appengine"
fi

# export SDKMAN_DIR="$H/.sdkman"
# [ -s "$H/.sdkman/bin/sdkman-init.sh" ] && source "$H/.sdkman/bin/sdkman-init.sh"


# android
export ANDROID_HOME=$W/.android-sdk

